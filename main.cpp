#include <bitset>

using std::bitset;
using std::string;

// написати функцію, яка повертає значення заданного біта у цілому числі
int getBit(int n, int pos) {
    return bitset<32>(n).test(pos);
}

// написати функцію, яка буде вмикати (вимикати) заданий біт у цілому числі
void switchBit(int &n, int pos) {
    n = (int) bitset<32>(n).flip(pos).to_ulong();
}

// використувуючи попередні пункти написати конвертер з бітового представлення у десятковий і навпаки
int bitsToInt(bitset<32> bits) {
    return (int) bits.to_ulong();
}

bitset<32> intToBits(int x) {
    return {(unsigned long long) x};
}


int main() {
    return 0;
}
